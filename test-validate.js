const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module', () => {
    context('Function isUserNameValid', () => {
        it('Function prototype : boolean isUserNameValid(username: String)', () => {
            expect(validate.isUserNameValid('kob')).to.be.true;
        });
        it('Characters at less than 3 characters', () => {
            expect(validate.isUserNameValid('tu')).to.be.false;
        });
        it('All characters are lower character', () => {
            expect(validate.isUserNameValid('Kob')).to.be.false;
            expect(validate.isUserNameValid('koB')).to.be.false;
        });
        it('Characters not more than 15 characters', () => {
            expect(validate.isUserNameValid('kob123456789012')).to.be.true;
            expect(validate.isUserNameValid('kob1234567890123')).to.be.false;
        });
    });

    context('Function isAgeValid', () => {
        it('Function prototype : boolean isAgeValid (age: String)', () => {
            expect(validate.isAgeValid('18')).to.be.true;
        });
        it('Age must be an Integer', () => {
            expect(validate.isAgeValid('a')).to.be.false;
        });
        it('Age must at less 18 years old and not more than 100 years old', () => {
            expect(validate.isAgeValid('17')).to.be.false;
            expect(validate.isAgeValid('18')).to.be.true;
            expect(validate.isAgeValid('100')).to.be.true;
            expect(validate.isAgeValid('101')).to.be.false;
        });
    });

    context('Function isPasswordValid', () => {
        it('Function prototype : boolean isUserNameValid(password: String)', () => {
            expect(validate.isPasswordValid('Pass|123')).to.be.true;
        });
        it('Password must have at less than 8 characters', () => {
            expect(validate.isPasswordValid('pass')).to.be.false;
        });
        it('Password must have at less than 1 character is upper charater', () => {
            expect(validate.isPasswordValid('password')).to.be.false;
            expect(validate.isPasswordValid('PassworD')).to.be.true;
        });
        it('Password must have digit more than 3 digit', () => {
            expect(validate.isPasswordValid('Passw12')).to.be.false;
            expect(validate.isPasswordValid('Passw123')).to.be.true;
            expect(validate.isPasswordValid('Passw1234')).to.be.true;
        });
        it('Password must have at less than 1 special character', () => {
            expect(validate.isPasswordValid('Passw12')).to.be.false;
            expect(validate.isPasswordValid('Pass|123')).to.be.true;
            expect(validate.isPasswordValid('Pass|1234|')).to.be.true;
        });
    });

    context('Function isDateValid', () => {
        it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)', () => {
            expect(validate.isDateValid (23,3,1999)).to.be.true;
        });
        it('Day must start with 1 and end not more than 31', () => {
            expect(validate.isDateValid (0,3,1999)).to.be.false;
            expect(validate.isDateValid (32,3,1999)).to.be.false;
        });
        it('Month must start with 1 and end not more than 12', () => {
            expect(validate.isDateValid (23,0,1999)).to.be.false;
            expect(validate.isDateValid (23,13,1999)).to.be.false;
        });
        it('Year must not less than 1970 and not more than 2020', () => {
            expect(validate.isDateValid (23,3,1969)).to.be.false;
            expect(validate.isDateValid (23,13,2021)).to.be.false;
        });
        it('Each month did not have equal days', () => {
            expect(validate.isDateValid (32,1,1999)).to.be.false;
            expect(validate.isDateValid (29,2,1999)).to.be.false;
            expect(validate.isDateValid (31,4,1999)).to.be.false;
        });
        it('Leap year', () => {
            expect(validate.isDateValid (29,2,1999)).to.be.false;
            expect(validate.isDateValid (30,2,2000)).to.be.false;
        });
    });
});